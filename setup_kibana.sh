#!/bin/bash

# Load environment variables
source .env

# Wait for Elasticsearch to start up before doing anything.
echo "Waiting for Elasticsearch to start..."
until curl -x "" -u ${ES_USER}:${ES_PASSWORD} $ES_URL; do
    sleep 2
    echo "Elasticsearch is starting... Checking at $ES_URL"
done

# Get Kibana access token
echo "Getting Kibana access token..."
output=$(curl -X POST -u $ES_USER:$ES_PASSWORD -x "" "$ES_URL/_security/service/elastic/kibana/credential/token/kibana-token")

echo $output

if [[ $output == *"version_conflict_engine_exception"* ]]; then
        echo "A token already has been created, skipping the sed and docker restart commands..."
else
    access_token=$(echo $output | sed -n 's/.*"value":"\([^"]*\)".*/\1/p')

    # Replace the token in kibana.yml
    sed -i "s/^elasticsearch.serviceAccountToken:.*$/elasticsearch.serviceAccountToken: $access_token/" kibana.yml

    echo "Restarting the Kibana Docker container..."
    docker-compose restart kibana
fi

# Check Kibana status and import dashboard once up
while true; do
    response=$(curl -X GET -s -x "" -u $ES_USER:$ES_PASSWORD "$KIBANA_URL/api/status")
    if echo "$response" | grep -q "Kibana server is not ready yet"; then
        echo "Kibana instance almost ready !"
        sleep 20
    else
        echo "Waiting for Kibana..."

        dashboard_response=$(curl -X POST -u $ES_USER:$ES_PASSWORD -x "" ${KIBANA_URL}/api/saved_objects/_import -H "kbn-xsrf: true" --form file=@./dashboard.ndjson)

        if echo "$dashboard_response" | grep -q '"success":true'; then
            echo "Saved dashboard imported successfully."
            echo "Script completed successfully."
            break
        else
            echo "Failed to import dashboard. Retrying..."
            sleep 8
        fi
    fi
done