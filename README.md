# ELK Stack

## Introduction

Welcome to this Docker Compose based ELK (Elasticsearch, Logstash, Kibana) stack project. This powerful platform, complemented by Filebeat, offers a unified solution for log analysis and visualization.

The ELK Stack combines Elasticsearch, Logstash, and Kibana, which together provide a robust tool for real-time log management and analysis. Integrated with Filebeat for log shipping, it becomes an essential tool for developers and system engineer.

This setup centralizes logs from different sources, making search, analysis, and visualization a breeze. This brings valuable insights to your application's usage and performance, helping in rapid issue identification and debugging.

## System Requirements and Recommendations

Do note, however, the ELK Stack can be resource-intensive, particularly for larger deployments. Be sure to plan resources adequately, considering both log volume and user count.

We recommend at least the following system requirements:

- 4 GB of RAM. For larger deployments or heavy workloads, consider allocating more RAM to avoid performance bottlenecks.
- Disk space of at least 50 GB. Depending on the volume of your logs, you may require more storage.
- A multi-core CPU for better performance.

Keep in mind, these are just baseline recommendations. Depending on your specific use case, you might need to scale up or down.

For ELK stack deployment, always start in a non-production environment for safe testing and troubleshooting.

Understand that it's resource-intensive, and unexpected usage spikes could impact existing systems. Transition to production only when confident in its test performance.

Note: deployment is at your own risk, ensuring readiness and managing potential disruptions is crucial.

## Pre-configuration

### For Ubuntu

Update your existing list of packages:
`sudo apt-get update`

Install the docker package :
`sudo apt-get install docker.io`

To install Docker Compose:
`sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`

Set the permissions:
`sudo chmod +x /usr/local/bin/docker-compose`

### For CentOS

Update your existing list of packages:
`sudo yum update`

Install Docker:
`sudo yum install docker`

Start Docker:
`sudo systemctl start docker`

To install Docker Compose:
`sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`

Set the permissions:
`sudo chmod +x /usr/local/bin/docker-compose`

Please note that Docker Compose version and Linux distribution can affect these commands. Always check the official Docker documentation for the most up-to-date instructions.

## Installation instructions

- Clone the Repository

  - `git clone https://gitlab.com/universis/universis-elk-kibana.git`

  - `cd elk-stack`

- Run Docker Compose

  - `docker-compose up -d`

- Setup Kibana and dashboard

  - `chmod +x setup_kibana.sh`

  - `./setup_kibana.sh`

## Access to Kibana

Once everything is up and running, you can view your logs in the Kibana web interface.

Open a web browser and navigate to `http://your-ip:5601`.

## Configuration

### Environment Variables

Please configure these in the .env file.

- ES_USER: Elasticsearch username
- ES_PASSWORD: Elasticsearch password
- ES_URL: Elasticsearch URL
- KIBANA_URL: Kibana URL

### Change the password

If you want to change the password to access the stack, you will have to change it in two file : 
- `.env` file where most of the data are stored
- `logstash.conf` in the output part, who is the only file who cannot read .env variable

### The ELK Stack

- Filebeat: Sends logs to Logstash for processing.
- Logstash: The server component of the ELK stack that processes incoming logs through filters.
- Elasticsearch: The database that will store your logs incoming from Logstash.
- Kibana: A web interface for searching and visualizing logs stored in Elasticsearch.

### Data Persistence

Data in Elasticsearch is kept persistent by using Docker volumes. The volume named esdata is linked to the Elasticsearch container.

This ensures that even if the container is stopped or deleted, the data will not be lost and will be available when the container is brought back up.

### Networking

The Docker Compose file sets up a custom network called elk. All the services in the stack communicate with each other over this network.

## Customization

Understanding the working of the ELK stack is crucial for customizing it to suit your needs. Here, we'll discuss how the stack operates and how to customize each component.

### Filebeat

Filebeat is a lightweight shipper for forwarding and centralizing log data. Installed as an agent on your servers, Filebeat monitors the log files or locations that you specify, collects log events, and forwards them to Logstash.

- Customize: Filebeat can be customized using the filebeat.yml configuration file. Here, you can specify the paths to monitor for log files, setup log rotation, and define output. Note that with Filebeat, you have the possiblity to have the log files and the ELK stack on different servers.

### Logstash

Logstash is a server-side data processing pipeline that ingests, transforms, and sends data to your Elasticsearch.

- Customize: Customizing Logstash involves editing the logstash.conf file. You can specify custom filters in the filter section of the file, which allows you to structure and query the data more efficiently.

### Elasticsearch

Elasticsearch is the heart of the ELK Stack. It is a distributed, RESTful search and analytics engine capable of addressing a growing number of use cases. It stores and indexes data shipped by Filebeat and searched by Kibana.

- Customize: You can customize using the elasticsearch.yml configuration file. This file contains settings like the node name, cluster name, network settings, paths, and memory settings, among other things. The Elasticsearch reference provides a comprehensive list of settings.

### Kibana

Kibana provides a user-friendly web interface for visualizing Elasticsearch data. It enables developers to create dashboards with bar, line and scatter plots, or pie charts, and maps on top of large volumes of data.

- Customize: Kibana's settings can be customized by editing the kibana.yml configuration file. You can set options like the Elasticsearch URL, default application, logging verbosity, etc. For visual customizations, Kibana provides a rich set of tools to build visualizations and dashboards from the web interface itself.

### Integrating Other Beats

Elastic provides several other Beats apart from Filebeat, like Metricbeat for shipping system and service metrics, Auditbeat for audit data, etc. You can integrate these into your ELK stack based on your requirements.

Remember, whenever you make changes to any configuration, you need to restart the respective service or the entire stack for the changes to take effect.

Kibana provides a dedicated Stack Monitoring feature that lets you easily monitor your stack in real-time.

Customizing an ELK stack can be as simple as changing a few configuration options or as complex as writing your own Logstash filter plugin. But, the flexibility and power of the stack come from this ability to be customized to meet the needs of any use case.

## Troubleshooting

If you encounter issues while setting up or running the stack, check the following:

- Ensure Docker and Docker Compose are correctly installed.
- Check the Docker Compose log using `docker-compose logs` or `docker logs {service-name}`.
- The filebeat.yml configuration file needs the root:root rights to work.
- Ensure all paths in configuration files are correct, and the files exist on your host system.
- Make sure your system has sufficient resources, as running an ELK stack can be resource-intensive.

### Stack Schema

![ELK Stack](elk.png)

