input {
  beats {
    port => 5044
    type => "webserver"
  }
}

output {
  elasticsearch {
    hosts => ["elasticsearch:9200"]
    data_stream => "true"
    data_stream_type => "logs"
    data_stream_dataset => "generic"
    data_stream_namespace => "test01"
    user => "elastic"
    password => "testpass123"
  }
}

filter {
  if [type] == "webserver" {

    if [host] =~ /^(ws01|ws-stg01|universis-api|registrar)/ {
      mutate {
        add_tag => [ "api" ]
        id => "webserver_mutate_5"
      }
    }
    if [host] =~ /^(universis-api|registrar)/ {
        mutate {
          add_tag => [ "universis_stack" ]
        }
    }
    if [log][file][path] =~ "apache2\/access" or [log][file][path] =~ "httpd\/access" or [log][file][path] =~ "hpc.auth.gr_access_ssl.log" {
      grok {
        patterns_dir => ["/etc/logstash/patterns"]
        match => { "message" => "%{ENRICHEDAPACHE}" }
        match => { "message" => "%{COMBINEDAPACHELOG}" }
        match => { "message" => "%{GREEDYDATA} \[%{HTTPDERROR_DATE:timestamp}\] %{GREEDYDATA}" }
        id => "webserver_grok_1"
      }
      mutate {
        add_tag => [ "access_log" ]
        id => "webserver_mutate_11"
      }
    }
    if [log][file][path] =~ "nginx\/?\w*\/access" or [log][file][path] =~ /(universis|registrar)-api\/access/ {
      grok {
        patterns_dir => ["/etc/logstash/patterns"]
        match => { "message" => "%{IPORHOST:vhost}:%{POSINT:port} %{NGINXCOMMON}" }
        match => { "message" => "%{NGINXCOMMON}" }
        id => "webserver_grok_3"
      }
      mutate {
        add_tag => [ "access_log" ]
        id => "webserver_mutate_12"
      }
    }
    if "api" in [tags] and "access_log" in [tags] {
      if "universis_stack" in [tags] {
        # Handle parsing of api specific logs.
        if [log][file][path] =~ /(universis|registrar)-api\/access/ {
          grok {
            patterns_dir => ["/etc/logstash/patterns"]
            match => ["request", "%{UNIVAPI:api_route}"]
            id => "webserver_grok_5"
          }
          mutate {
            add_field => { "uri" => "%{api_route}" }
            id => "webserver_mutate_22"
          }
          mutate {
            gsub => [ "forwarder", "\"","", "referrer", "\"","" ]
            id => "webserver_mutate_2"
          }
          mutate {
            replace => [ "clientip", "%{forwarder}" ]
            id => "webserver_mutate_9"
          }
        } else {
          mutate {
            add_field => { "api_route" => "%{uri}" }
          }
        }
      } else {
        grok {
          patterns_dir => ["/etc/logstash/patterns"]
          match => ["request", "%{WSAPI:api_route}"]
          id => "webserver_grok_2"
        }
      }
    }
    if "ssl-error" not in [message] and [clientip] {
      geoip {
        source => "clientip"
        target => "geoip"
        id => "webserver_geoip_1"
      }
    }
    if [bytes] {
      mutate {
        convert => {
          bytes => "integer"
        }
        id => "webserver_mutate_1"
      }
    }
    if [duration] {
      mutate {
        convert => {
          duration => "integer"
        }
        id => "webserver_mutate_3"
      }
    }
    if [log][file][path] =~ /(universis|registrar)-api\/error/ {
      grok {
        patterns_dir => ["/etc/logstash/patterns"]
        match => { "message" => "%{UNIVERSISERRDATE},%{QS},(%{UNIVERSISTRACEUSER},)?(%{UNIVERSISTRACEREQ},)?%{GREEDYDATA:[labels][fields]}" }
        id => "webserver_grok_4"
      }
      if [request] {
        grok {
          patterns_dir => ["/etc/logstash/patterns"]
          match => ["request", "%{WSAPI:api_route}"]
        }
      }
      mutate {
        gsub => [ "[labels][fields]", "[{}\[\]\"]", "" ]
        gsub => [ "[labels][fields]", "\, ", " " ]
        id => "webserver_mutate_13"
      }
      kv {
        allow_duplicate_values => false
        source => "[labels][fields]"
        target => "[labels]"
        remove_char_value => "'"
        value_split => ":"
        field_split => ","
        transform_key => "lowercase"
        id => "webserver_kv_1"
      }
      if [labels][code] {
        mutate {
          add_field => { "[error][code]" => "%{[labels][code]}" }
          id => "webserver_mutate_15"
        }
      }
      if [labels][message] {
        mutate {
          add_field => { "[error][message]" => "%{[labels][message]}" }
          id => "webserver_mutate_16"
        }
      }
      if [labels][stack] {
        mutate {
          add_field => { "[error][stack_trace]" => "%{[labels][stack]}" }
          id => "webserver_mutate_17"
        }
      }
      if [labels][type] {
        mutate {
          add_field => { "[error][type]" => "%{[labels][type]}" }
          id => "webserver_mutate_18"
        }
      }
      if [labels][statuscode] {
        mutate {
          add_field => { "[http][response][status_code]" => "%{[labels][statuscode]}" }
          id => "webserver_mutate_19"
        }
      }
      mutate {
        rename => {
          "[labels][innermessage]" => "[labels][inner_message]"
          "[labels][additionaldata]" => "[labels][additional_data]"
          "[labels][requesterror]" => "[labels][request_error]"
          "[labels][statuscode]" => "[labels][full_status_code]"
        }
        id => "webserver_mutate_20"
      }
      mutate {
        convert => {
          "[labels][full_status_code]" => "string"
        }
        id => "webserver_mutate_21"
      }
    }
    date {
      match => [ "timestamp" , "dd/MMM/yyyy:HH:mm:ss Z", "EEE MMM dd HH:mm:ss.SSSSSS yyyy" ]
      id => "webserver_date_1"
    }
    mutate {
      remove_tag => ["apache","nginx","api","access_log","universis_stack"]
      remove_field => [ "timestamp", "forwarder", "[labels][code]", "[labels][message]", "[labels][stack]", "[labels][type]", "[labels][fields]" ]
      id => "webserver_mutate_4"
    }
  }
}