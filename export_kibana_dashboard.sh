#!/bin/bash

# Load environment variables
source .env

# Retrive the dashboard as dashboard.ndjson
curl -X POST -x "" "${KIBANA_URL}/api/saved_objects/_export" \
  -H 'kbn-xsrf: true' \
  -H 'Content-Type: application/json' \
  -u ${ES_USER}:${ES_PASSWORD} \
  -d '{
      "includeReferencesDeep": true,
      "objects": [
        {
          "type": "dashboard",
          "id": "DASHBOARD_ID"
        }
      ]
    }' > dashboard.ndjson

echo "Done"